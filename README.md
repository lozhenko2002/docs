# Contents
+ [Campaign](https://gitlab.com/lozhenko2002/docs/-/blob/main/Docs/definitions.md#Campaign)
    * [Adding orders to a campaign](https://gitlab.com/lozhenko2002/docs/-/blob/main/Docs/add_orders.md)
    * [Getting order statuses](https://gitlab.com/lozhenko2002/docs/-/blob/main/Docs/get_status.md)
    * [Obtaining information on the history of order actions](https://gitlab.com/lozhenko2002/docs/-/blob/main/Docs/get_bpm_actions.md)
    * [Receiving information on order calls](https://gitlab.com/lozhenko2002/docs/-/blob/main/Docs/get_calls.md)
    * [Getting information on text messages by order](https://gitlab.com/lozhenko2002/docs/-/blob/main/Docs/get_messages.md)
+ [Definitions in the documentation](https://gitlab.com/lozhenko2002/docs/-/blob/main/Docs/definitions.md)
