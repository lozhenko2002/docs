# Adding orders to a campaign
``` /api/v2/bpm/public/bp/{api_key}/add_orders ```

## Description:
The method adds [order](https://gitlab.com/acsolutions/docs/-/blob/main/docs/definitions.md#%D0%BB%D0%B8%D0%B4) to the campaign <br/>
Limit 10 orders per addition.

**api_key** - is a key from the API on the campaign page.

To add orders, send the following parameters to ``` /api/v2/bpm/public/bp/{api_key}/add_orders ``` by POST method.

| Variable  | Value | Necessity |
| ------ | ------ | ------ |
| phone | Phone number | Mandatory |
| full_name | Name | Optional |
| sex | Gender: *0 - Undefined; 1 - Man; 2 - Woman* | Optional |
| race | Nationality: *0 - Undefined; 1 - Kazakh; 2 - Russian* | Optional |
| age | Age | Optional |
| web_site | Url website | Optional, will link to the order |
```
[
    {
       "phone": 79110000008,
       "full_name": "Test 3",
       "web_site": "http://site555.ru/"
    }
]
```
Response options:
| Variable  | Value |
| ------ | ------ |
| contrahen | Counterparty key |
| order | Order key |


Sample answer:
```
{
   "data": {
       "79110000008": {
           "contrahen": "9c65fe123e",
           "order": "e3b8950f6b"
       },
       "79110000009": {
           "contrahen": "8464b271e7",
           "order": "0f4fb50996"
       }
   }
}
```
