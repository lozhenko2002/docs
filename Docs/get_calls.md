# Getting information on order calls.
``` /api/v2/orders/public/{api_key}/get_calls ```

## Description:
The method returns information on [calls](https://gitlab.com/acsolutions/docs/-/blob/main/docs/definitions.md#%D0%B7%D0%B2%D0%BE%D0%BD%D0%BA%D0%B8) that are tied to [order](https://gitlab.com/acsolutions/docs/-/blob/main/docs/definitions.md#%D0%BB%D0%B8%D0%B4).

Limit 10 keys per request.

**api_key** - is a key from the API on the campaign page.

To receive calls you need to send to ``` /api/v2/orders/public/{api_key}/get_calls ``` the following parameters using the GET method.

| Variable  | Value | Necessity |
| ------ | ------ | ------ |
| keys | Order keys listed separated by commas. | Mandatory |
| from | From what date to get the data. Format: Y-m-d | Optional |
| to | To what date to get the data. Format: Y-m-d | Optional |

Response options:

| Variable  | Value |
| ------ | ------ |
| order_key | Order key |
| link | Link to the recording of the conversation |
| duration | Call duration |
| time | Call Date |
| type | Type of call: *auto - Automatic call; bot - Outgoing call by robot; in - Incoming call; bot_incall - Incoming call processed by robot; out - Outgoing call by human* |

Example request
``` /api/v2/orders/public/XXXXXXXXXX/get_calls?keys=XXXXX,YYYYY ```

Sample answer
```
[
   {
       "order_key": "XXXXX",
       "link": "https://rec.dstcrm.online/2019/06/25/123456789.123456.mp3",
       "duration": null,
       "time": "2019-06-25 17:58:45",
       "type": "auto"
   },
   {
       "order_key": "XXXXX",
       "link": "https://rec.dstcrm.online/2019/06/25/987654321.654321.mp3",
       "duration": null,
       "time": "2019-06-25 18:17:30",
       "type": "auto"
   },
   {
       "order_key": "YYYYY",
       "link": "https://rec.dstcrm.online/2019/06/25/1122334455.456789.mp3",
       "duration": null,
       "time": "2019-06-25 18:50:52",
       "type": "auto"
   }
]
```
