# Getting order statuses
``` /api/v2/orders/public/{api_key}/get_status ```

## Description:
This api method allows you to get the current [statuses](https://gitlab.com/acsolutions/docs/-/blob/main/docs/definitions.md#%D1%81%D1%82%D0%B0%D1%82%D1%83%D1%81) [order](https://gitlab.com/acsolutions/docs/-/blob/main/docs/definitions.md#%D0%BB%D0%B8%D0%B4) by order keys

Limit 10 orders per request.

**api_key** - is a key from the API on the campaign page.

To get the statuses you need to send to ``` /api/v2/orders/public/{api_key}/get_status ``` the following parameters using the GET method.

| Variable  | Value | Necessity |
| ------ | ------ | ------ |
| keys | Order keys listed separated by commas. | Mandatory |
| lang | Localization of status names. The default is ru. | Optional |

Response options:

| Variable  | Value |
| ------ | ------ |
| status_group_5 | Status group |
| status_id | Status ID |
| name | Status name |

Example request
``` /api/v2/orders/public/XXXXXXXXXX/get_status?keys=XXXXX,YYYYY ```

Example response:
```
{
   "XXXXX": {
       "status_group_5": {
           "status_id": 10, // Status ID
           "name": "on review" // Status name
       },
       "status_group_1": {
           "status_id": 11,
           "name": "verified"
       }
   },
   "YYYYY": {      
       "status_group_1": {
           "status_id": 12,
           "name": "awaits"
       }
   }
}
 ```
