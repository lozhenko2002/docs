# Obtaining information on the history of order actions.
``` /api/v2/orders/public/{api_key}/get_bpm_actions ```

# Description:
When contacted, you will be able to see the history of transitions that occurred with [order](https://gitlab.com/acsolutions/docs/-/blob/main/docs/definitions.md#%D0%BB%D0%B8%D0%B4) in-campaign

Limit 10 keys per request.

**api_key** - is a key from the API on the campaign page.

To get [statuses](https://gitlab.com/acsolutions/docs/-/blob/main/docs/definitions.md#%D1%81%D1%82%D0%B0%D1%82%D1%83%D1%81) you need to send to ``` /api/v2/orders/public/{api_key}/get_bpm_actions ``` the following parameters using the GET method.

| Variable  | Value | Necessity |
| ------ | ------ | ------ |
| keys | Order keys listed separated by commas. | Mandatory |

Response options:

| Variable  | Value |
| ------ | ------ |
| id | Action ID |
| title | Name of action |
| run_time | Start time |
| end_time | Time of action completion |
| jump_id | Transition ID, on which the order was transferred at the end of the action |
| jump_title | Name of transition |
| next_action_id | Next action |

Example request
``` /api/v2/orders/public/XXXXXXXXXX/get_bpm_actions?keys=XXXXX,YYYYY,LLLLL ```

Example response:
*The order of actions in the answer, sorted from extreme to starting.*
```
{
   "LLLLL": [],
   "XXXXX": [
       {
           "id": 298,  
           "title": "Transfer to another BP 1",  
           "run_time": "2022-02-10 15:18:42",
           "end_time": "2022-02-10 15:19:09",
           "jump_id": null,  
           "jump_title": null,
           "next_action_id": null
       },
       {
           "id": 307,
           "title": "Order confirmation. 2",
           "run_time": "2022-02-10 15:10:59",
           "end_time": "2022-02-10 15:16:11",
           "jump_id": 484,
           "jump_title": "Confirmed 4",
           "next_action_id": 298
       },
       {
           "id": 300,
           "title": "Sale with an improved offer",
           "run_time": "2022-02-10 14:55:56",
           "end_time": "2022-02-10 15:04:31",
           "jump_id": 473,
           "jump_title": "Interested 2",
           "next_action_id": 307
       },
       {
           "id": 299,
           "title": "Identifying interest",
           "run_time": "2022-02-10 14:32:18",
           "end_time": "2022-02-10 14:54:56",
           "jump_id": 472,
           "jump_title": "Refusal 1",
           "next_action_id": 300
       }
   ],
   "YYYYY": [
       {
           "id": 335,
           "title": "Refusal",
           "run_time": "2022-02-25 09:30:45",
           "end_time": "2022-02-25 09:47:54",
           "jump_id": null,
           "jump_title": null,
           "next_action_id": null
       },
       {
           "id": 333,
           "title": "Distribution",
           "run_time": "2022-02-25 09:00:58",
           "end_time": "2022-02-25 09:30:44",
           "jump_id": 522,
           "jump_title": "TEST 111",
           "next_action_id": 335
       }
   ]
}
```
