### Campaign
- Campaign is a dashboard for managing your contacts and getting extensive statistics on them


#### Text messages
- The messages we send to the client on watsap.
- Text messages can be regular or template messages. Template messages are welcoming (first) messages with which a dialogue begins. They must be approved in advance by watsap, or rather by a special service (in our case it is 360dialog).
- Also according to the semantic load text messages are divided into ordinary and mailings. Mailings - this is most often a template message, which bears a notifying purpose, rarely two messages, where the second - a farewell or addition to the first message. 


#### Action
- An "action" refers to a unit of work performed in the course of campaign work.
- Types of actions:
1. **queue *(Outgoing queue)*** - Our operator is calling.
2. **in *(Incoming Queue)*** - A person calls us.
3. **callbot *(Call robot)*** - The robot is calling.
4. **bot_incall *(Incoming robot)*** - A person calls a robot.
5. **chatbot *(Chatbot)*** - We use a chatbot to write to the client.
6. **manual *(Manual action)*** - Goes into the unloading, check, put the status.
7. **transfer *(Transfer to another BP)*** - We have to send the order to another BP.
8. **split *(Split Test)*** - Orders need to be divided up and let into different crosswalks.
9. **sms_template *(Sending SMS)*** - We send SMS to the person.
10. **segment *(Segment)*** - BP is over, we need to identify the order, put it an ID depending on the condition.
11. **gateway *(Gateway)*** - Conditional designation "or", condition check. By gateways we understand the elements that define branching and merging of work flows.

- Actions are needed to make certain manipulations on the order and to move the order through the script.


#### Order 
- An appeal from a customer that needs to be processed.
- The order is necessary to specify the data necessary for its processing (statuses, contact and other information).
- Orders are created via API, downloaded from files, manually created by managers, created from incoming call. 


#### Calls
- Voice communication with the client.
- What kind of calls there are (Possible values of the type field):


1. **out *(Outgoing call by human)*** - Such a call is initiated manually by a person, such calls can be made from the user's softphone, or from an order in the system.  
2. **auto *(Automatic call)*** - The call is initiated by a queue, in which contacts are handled by humans, without robots. The queue automatically dials the required number of customers, by the number of available operators. 
3. **bot *(Outgoing call by robot)*** - A call initiated by a robot on a list of contacts that this robot has. 
4. **bot_in *(Incoming call processed by robot)*** - Call initiated by the client, the client made an incoming call to the number, calls from which are handled by the robot.
5. **in *(Incoming call)*** - A call initiated by a customer to a number from which calls are handled by humans without the involvement of robots.


#### Status
- The state in which the order is at the result of its processing at a particular stage. 


#### Status group
- The contact processing step, which requires setting a unique order status value.
