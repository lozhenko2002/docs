# Getting information on order text messages.
``` /api/v2/orders/public/{api_key}/get_messages ```

## Description:
The method returns [text messages](https://gitlab.com/acsolutions/docs/-/blob/main/docs/definitions.md#%D1%82%D0%B5%D0%BA%D1%81%D1%82%D0%BE%D0%B2%D1%8B%D0%B5-%D1%81%D0%BE%D0%BE%D0%B1%D1%89%D0%B5%D0%BD%D0%B8%D1%8F) which are tied to [order](https://gitlab.com/acsolutions/docs/-/blob/main/docs/definitions.md#%D0%BB%D0%B8%D0%B4). 

Limit 10 keys per request.

**api_key** - is a key from the API on the campaign page.

To receive text messages you need to send to ``` /api/v2/orders/public/{api_key}/get_messages ``` the following parameters using the GET method.

| Variable  | Value | Necessity |
| ------ | ------ | ------ |
| keys | Order keys listed separated by commas. | Mandatory |
| from | From what date to get the data. Format: Y-m-d | Optional |
| to | To what date to get the data. Format: Y-m-d | Optional |

Response options:

| Variable  | Value |
| ------ | ------ |
| order_key | Order key |
| type | Message type: *sms - SMS message; chatbot - chatbot message* |
| author | Sender: *bot - Automatic sending; user - Manual sending by the user* |
| text | Message Text |
| created_at | Date of shipment |
| action_id | ID Actions, if any |
| action_title | The name of the action, if any. |
| bpm_bp_key | Business process key, if any |
| bpm_bp_title | Name of the business process, if any |

Example request
``` /api/v2/orders/public/XXXXXXXXXX/get_messages?keys=XXXXX,YYYYY ```

Sample answer
```
[
   {
       "order_key": "XXXXX",
       "type": "sms",
       "author": "bot",
       "text": "Dear customer! The parcel has arrived and is in the courier service.",
       "created_at": "2021-02-17 15:04:39",
       "action_id": null,
       "action_title": null,
       "bpm_bp_key": null,
       "bpm_bp_title": null
   },
   {
       "order_key": "XXXXX",
       "type": "sms",
       "author": "user",
       "text": "Good afternoon! E-mail client@gmail.com for returns and exchanges",
       "created_at": "2021-05-05 15:03:06",
       "action_id": null,
       "action_title": null,
       "bpm_bp_key": null,
       "bpm_bp_title": null
   },
   {
       "order_key": "YYYYY",
       "type": "chatbot",
       "author": "bot",
       "text": "1",
       "created_at": "2022-05-03 14:51:17",
       "action_id": 1246,
       "action_title": "Chatbot",
       "bpm_bp_key": "7dfb54b68d",
       "bpm_bp_title": "test 05-03"
   }
]
```
